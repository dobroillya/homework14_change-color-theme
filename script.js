const btn = document.querySelector('.change-theme');
const container = document.querySelector('.container');
localStorage.setItem('theme', 'theme-default');

function setTheme() {  
    console.log(container)  
    container.classList.add (localStorage.getItem('theme'));
}

setTheme();

function toggle(){
    if (localStorage.getItem('theme') === 'theme-default'){
        localStorage.setItem('theme', 'theme-change');
        container.classList.remove('theme-default');
    } else {
        localStorage.setItem('theme', 'theme-default');
        container.classList.remove('theme-change');
    }
    setTheme();
}


btn.addEventListener('click', ()=>{    
    
    toggle();
})